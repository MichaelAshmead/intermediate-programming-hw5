//hw5 header file

#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <list>
#include <cstdbool>
#include <ctime>

using namespace std;


typedef unordered_map<string, int > s2Int;

struct strfollow {
	string followingString;
	int pairAmount;
};

typedef strfollow strFollowStruct;
typedef unordered_map<string, list <strFollowStruct>> strFollowMap;

void initVect(vector<string> (& vin));
void initMaps(vector <string> v, s2Int & stringoccur, strFollowMap & followMap);
vector<string> generateWords(vector <string> v, s2Int & stringoccur, strFollowMap & followMap, unsigned long size);
void printwords(vector<string> allwords);
