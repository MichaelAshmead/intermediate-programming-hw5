/*
Michael Ashmead, Alex Owen, 600.120, 4/1/15, Homework 5, 484-682-9355, 704-661-8270, mashmea1, aowen10, ashmeadmichael@gmail.com, aowen10@jhu.edu
*/

//hw5 mainfile
#include "header.h"

int main() {
	s2Int stringoccur;
	strFollowMap followMap;
	string tempcur;
	vector<string> v;
	initVect(v);
	unsigned long size = v.size();
	
	initMaps(v, stringoccur, followMap);

	if (size == 0) {
		return -1;
	}
	
	unsigned long c=0;
	string tempprev = v[c];
	//looks at pairs of words and counts how many times they occurred
	while ((c+1) < size) {
		tempcur = v[c + 1];
		if (stringoccur.find(tempprev) == stringoccur.end()) {
			//create a new element with key value of tempprev, set the mapped value to 1
			stringoccur.emplace(tempprev, 1);
			strFollowStruct newFollowing;
			newFollowing.pairAmount = 1;
			newFollowing.followingString = tempcur;
			followMap[tempprev].push_front(newFollowing);
		}
		else {
			stringoccur[tempprev]++; //increase the mapped value by 1
			bool stopSearch = false;
			list <strFollowStruct>::iterator iter = followMap[tempprev].begin();
			while (!stopSearch && iter != followMap[tempprev].end()) {
				if (iter->followingString == tempcur) {
					(iter->pairAmount)++;
					stopSearch = true;
				}
				iter++;
			}
			if (!stopSearch) {
				strFollowStruct newFollowing;
				newFollowing.pairAmount = 1;
				newFollowing.followingString = tempcur;
				followMap[tempprev].push_back(newFollowing);
			}
		}
		tempprev = tempcur;
		c++;
	}


	/*cout << "Printing Pairs: " << endl;
	for (auto& x: stringoccur) {
    		cout << x.first << ": " << x.second << endl;
  	}	

	for (auto &y: followMap) {
		cout << "Words following \"" << y.first << "\":" << endl;
  		list <strFollowStruct>:: iterator Iter = y.second.begin();
  		while (Iter != y.second.end()) {
  			cout << "\"" << Iter->followingString << "\" " << Iter->pairAmount << " time(s)" << endl;
  			Iter++;
	  	}
		cout << endl;
	}*/

	vector<string> allwords = generateWords(v, stringoccur, followMap, size);

	printwords(allwords);
	
	return 0;
}
