/*
Michael Ashmead, Alex Owen, 600.120, 4/1/15, Homework 5, 484-682-9355, 704-661-8270, mashmea1, aowen10, ashmeadmichael@gmail.com, aowen10@jhu.edu
*/

//helper.cpp

#include "header.h"

void initVect(vector <string> & vin){
	cout << "Please input your sentence/phrase followed by ENTER then CTRL+D:" << endl;
	string tempcur = "<START>";
	while(!cin.eof()){
		vin.push_back(tempcur);
		cin>> tempcur;
	}
	vin.push_back("<END>");
}

void initMaps(vector <string> v, s2Int & stringoccur, strFollowMap & followMap){
	//looks at pairs of words and counts how many times they occurred
	unsigned long c=0;
	string tempprev= v[c];
	string tempcur;
	while ((c+1)< v.size()) {
		tempcur = v[c +1];
		if (stringoccur.find(tempprev) == stringoccur.end()) {
			//create a new element with key value of tempprev, set the mapped value to 1
			stringoccur.emplace(tempprev, 1);
			strFollowStruct newFollowing;
			newFollowing.pairAmount = 1;
			newFollowing.followingString = tempcur;
			followMap[tempprev].push_front(newFollowing);
		}
		else {
			stringoccur[tempprev]++; //increase the mapped value by 1
			bool stopSearch = false;
			list <strFollowStruct>::iterator iter = followMap[tempprev].begin();
			while (!stopSearch && iter != followMap[tempprev].end()) {
				if (iter->followingString == tempcur) {
					(iter->pairAmount)++;
					stopSearch = true;
				}
				iter++;
			}
			if (!stopSearch) {
				strFollowStruct newFollowing;
				newFollowing.pairAmount = 1;
				newFollowing.followingString = tempcur;
				followMap[tempprev].push_back(newFollowing);
			}
		}
		tempprev = tempcur;
		c++;
	}

}

vector<string> generateWords(vector <string> v, s2Int & stringoccur, strFollowMap & followMap, unsigned long size) {
	//generate random word output
	vector<string> allwords;
	string generatedWord = "<START>";
	srand(time(NULL)); //needed to seed rand()

	while (generatedWord != v[size-1]) {
		int random = rand() % stringoccur[generatedWord];
		//build array of words that follow generatedWord
		vector<string> vFollowWords;
			
		list<strFollowStruct> x = followMap[generatedWord];
  		list<strFollowStruct> :: iterator xIter = x.begin();

  		while (xIter != x.end()) {
			for (int x = 0; x < xIter->pairAmount; x++) {
				vFollowWords.push_back(xIter->followingString);
  			}
  			xIter++;
	  	}
		generatedWord = vFollowWords[random];
		if (generatedWord != v[size-1])
			allwords.push_back(generatedWord);
	}
	return allwords;
}

void printwords(vector<string> allwords) {
  	vector<string> :: iterator iter = allwords.begin();

  	while (iter != allwords.end()) {
		cout << *iter << " ";
  		iter++;
	}
	cout << endl;
}
