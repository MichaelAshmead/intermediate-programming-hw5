/*
Michael Ashmead, Alex Owen, 600.120, 4/1/15, Homework 5, 484-682-9355, 704-661-8270, mashmea1, aowen10, ashmeadmichael@gmail.com, aowen10@jhu.edu
*/

#include "header.h"
#include <cassert>

int main() {
	s2Int stringoccur;
	strFollowMap followMap;

	std::vector<string> v;
	cout<< "Testing initVect" << endl;
	initVect(v);
	std::vector<string> testv { "<START>", "he","thought", "that", "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked", "<END>" };
	
	for (unsigned long i = 0; i < v.size(); i++)
	{
		cout <<v[i] << endl;
		assert(v[i]== testv[i]);
	}
	cout<< "Testing initMaps" << endl;
	initMaps( v, stringoccur, followMap);
	
	
	assert( stringoccur["<START>"] == 1);
    assert( stringoccur["he"] == 4);
    assert( stringoccur["liked"] == 2);
    assert( stringoccur["that"] == 2);
    assert( stringoccur["ate"] == 1);
    assert( stringoccur["the"] == 1);
    assert( stringoccur["sandwich"] == 1);
    assert( stringoccur["<END>"] == 0);

	
    assert( followMap["<START>"].size() == 1);
    assert( followMap["he"].size() == 3);
    assert( followMap["liked"].size() == 2);
    assert( followMap["that"].size() == 1);
    assert( followMap["ate"].size() == 1);
    assert( followMap["the"].size() == 1);
    assert( followMap["sandwich"].size() == 1);
    assert( followMap["<END>"].size() == 0);


	list <strFollowStruct>::iterator iter = followMap["<START>"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "he");
	iter = followMap["he"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "thought");
	iter++;
	assert(iter->pairAmount == 2);
	assert(iter->followingString== "liked");
	iter++;
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "ate");

	iter = followMap["liked"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "that");
	iter++;
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "<END>");
	iter = followMap["that"].begin();
	assert(iter->pairAmount == 2);
	assert(iter->followingString== "he");
	iter = followMap["ate"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "the");
	iter = followMap["the"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "sandwich");
	iter = followMap["sandwich"].begin();
	assert(iter->pairAmount == 1);
	assert(iter->followingString== "he");

	
	return 0;
}
