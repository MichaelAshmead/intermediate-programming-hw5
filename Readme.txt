Authors: Michael Ashmead, Alex Owen
JHED ID: mashmea1, aowen10
Emails: mashmea1@jhu.edu, aowen10@jhu.edu
Phone #s: 4846829355, 7046618270
Date: 4/1/15

Example input after running make in command line:  echo "a b a b a b b b b b a a a" | ./hw5

To test, use echo "he thought that he liked that he ate the sandwich he liked" | make test
