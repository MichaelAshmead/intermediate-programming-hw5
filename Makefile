# Makefile for HW5
#
# USAGE:
#
# //to compile:
# make
#
# //to compile tests and run tests:
# make test
#
# //remove compilation output files:
# make clean
#
# make variables let us avoid pasting these options in multiple places
CC = g++ 
CFLAGS = -std=c++11 -pedantic -Wall -Wextra -O    
#CFLAGS = -std=c++11 -pedantic -Wall -Wextra -O0 -stdlib=libstdc++ -g   #for debugging

bin: hw5

test: testfile
	echo "Running tests..."
	./testfile
	echo "All Tests Passed."

helper.o: helper.cpp header.h
	$(CC) $(CFLAGS) -c  helper.cpp

testfile.o: testfile.cpp header.h
	$(CC) $(CFLAGS) -c testfile.cpp

hw5.o: hw5.cpp header.h
	$(CC) $(CFLAGS) -pedantic -O -c  hw5.cpp

testfile: testfile.o helper.o
	$(CC) $(CFLAGS) -O -o testfile testfile.o helper.o

hw5: hw5.o helper.o
	$(CC) $(CFLAGS) -O -o hw5 hw5.o helper.o

clean:
	rm -f *.o hw5

